define ['backbone', 'underscore'], (Backbone, _)->
  window.App = _.extend {}, window.App, {
    View: Backbone.View.extend(),
    Model: Backbone.Model.extend(),
    Router: Backbone.Router.extend(),
  }
